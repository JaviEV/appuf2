package com.example.appuf2.ui.dashboard;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.appuf2.Product;
import com.example.appuf2.R;

public class ProductAdapter extends AppCompatActivity {

    private Product pro;
    private TextView nom,descrip,localitza,preu;
    private ImageView ima;
    private final Activity activity;

    public ProductAdapter(Activity activity){
        this.activity = activity;

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_product);

        pro = (Product) getIntent().getSerializableExtra("producto");
        View view = activity.getLayoutInflater().inflate(R.layout.info_product,null);

        ima = view.findViewById(R.id.foto);
        nom = view.findViewById(R.id.nom);
        descrip = view.findViewById(R.id.description);
        localitza = view.findViewById(R.id.localitzacio);
        preu = view.findViewById(R.id.price);

        nom.setText(pro.getNom());
        descrip.setText(pro.getDescripcio());
        localitza.setText(pro.getDescripcio());
        preu.setText(pro.getPreu());

        Glide.with(activity).load(pro.getUrl()).into(ima);


    }
}
