package com.example.appuf2.ui.dashboard;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.appuf2.Product;
import com.example.appuf2.R;
import com.example.appuf2.ui.home.HomeViewModel;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    private HomeViewModel homeViewModel;

    private List<Product> listProduct = new ArrayList<Product>();
    ArrayAdapter<Product> arrayAdaperProduct;
    private ArrayList<Product> array2 = new ArrayList<>();

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ListView listPro;


    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        listPro = root.findViewById(R.id.lvProducts);

        iniciarFirebase();

        listarDatos();

        return root;
    }

    private void listarDatos() {
        databaseReference.child("Producto").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listProduct.clear();
                for (DataSnapshot objSnapshot : dataSnapshot.getChildren()){
                    Product p = objSnapshot.getValue(Product.class);
                    if (getActivity() != null){
                        listProduct.add(p);
                        //array2.add(p);
                        arrayAdaperProduct = new ArrayAdapter<Product>(getContext(),android.R.layout.simple_list_item_1,listProduct);
                        listPro.setAdapter(arrayAdaperProduct);

                        /*

                        listPro.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getActivity(), ProductAdapter.class);
                                intent.putExtra("producto",array2.get(position));
                                startActivity(intent);

                            }
                        });

                         */
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void iniciarFirebase() {
        FirebaseApp.initializeApp(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

}
