package com.example.appuf2.ui.notifications;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.appuf2.CustomInfoWindowAdapter;
import com.example.appuf2.Product;
import com.example.appuf2.R;
import com.example.appuf2.ui.home.HomeViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment{
    private HomeViewModel homeViewModel;
    private DatabaseReference mDatabase;
    private ArrayList<Marker> tmpRealTimeMakers = new ArrayList<>();
    GoogleMap myGoogle;





    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LatLng aux;
        // Inflate the layout for this fragment
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");


        mapFragment.getMapAsync(map -> {
            // Codi a executar quan el mapa s'acabi de carregar.

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //fills node
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        Product pro = snapshot.getValue(Product.class);
                        CustomInfoWindowAdapter customInfoWindow = new CustomInfoWindowAdapter(
                                getActivity()
                        );
                        Double lati = Double.valueOf(pro.getLatitud());
                        Double longi = Double.valueOf(pro.getLongitud());
                        Marker marker = map.addMarker(new MarkerOptions()
                                .title(pro.getNom())
                                .snippet(pro.getDescripcio())
                                .position(new LatLng(lati,longi)));
                        marker.setTag(pro);
                        map.setInfoWindowAdapter(customInfoWindow);

                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        });





        return root;
    }




}
