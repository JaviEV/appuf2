package com.example.appuf2.ui.home;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.appuf2.MainActivity;
import com.example.appuf2.Product;
import com.example.appuf2.R;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private HomeViewModel homeViewModel;
    private Button bLocation;
    private static final int REQUEST_LOCATION_PERMISION = 1;
    private TextView textLatLong,textAddress;
    private ProgressBar progressBar;
    private EditText mEditText,mdes,mpreu;
    private DatabaseReference mDatabase;
    //----- storage
    private Button mUploadBtn;
    private StorageReference mStorage;
    private static final int GALLERY_INTENT = 1;
    private ImageView image;
    private ProgressDialog mProgressDialog;
    private static String url;



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_home, container, false);
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        bLocation = root.findViewById(R.id.buttonGetCurrentLocation);


        textLatLong = root.findViewById(R.id.textLatLong);
        progressBar = root.findViewById(R.id.progressBar);
        textAddress = root.findViewById(R.id.textAddress);
        mEditText = root.findViewById(R.id.editText);
        mdes = root.findViewById(R.id.descripcioText);
        mpreu = root.findViewById(R.id.preuText);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //----
        mUploadBtn = (Button)root.findViewById(R.id.botonFoto);
        mStorage = FirebaseStorage.getInstance().getReference();
        image = root.findViewById(R.id.foto);
        //--------
        mProgressDialog = new ProgressDialog(getContext());


        progressBar.setVisibility(View.GONE);

        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_INTENT);


            }
        });

        bLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check();

            }
        });


        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_INTENT && resultCode == getActivity().RESULT_OK){
            mProgressDialog.setTitle("Push photo");
            mProgressDialog.setMessage("Subiendo foto a firebase");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            Uri uri = data.getData();
            StorageReference filePath = mStorage.child("fotos").child(uri.getLastPathSegment());
            UploadTask uploadTask = filePath.putFile(uri);
            uploadTask.addOnSuccessListener(taskSnapshot -> {
                taskSnapshot.getMetadata();
                filePath.getDownloadUrl().addOnCompleteListener(task -> {
                    mProgressDialog.dismiss();
                    Uri dowlo =task.getResult();
                    Glide.with(getContext()).load(dowlo).into(image);

                    url = dowlo.toString();

                });

            });

        }

    }






    private void check(){
        Log.i("Myactivity","entra");
        progressBar.setVisibility(View.VISIBLE);

        if (ContextCompat.checkSelfPermission(this.getContext(),Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(this.getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION_PERMISION);
            getCurrentLocation();

        }else {
            Log.i("Myactivity","no permisos");

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("Myactivity","entra2");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISION && grantResults.length > 0 ){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation();
            }else {
                Toast.makeText(this.getContext(), "Permission denied", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void getCurrentLocation(){
        Log.i("Myactivity","entra3");
        progressBar.setVisibility(View.VISIBLE);

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(this.getContext())
                .requestLocationUpdates(locationRequest,new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(this);
                        if (locationResult != null && locationResult.getLocations().size() > 0){
                            int lastestLocationIndex = locationResult.getLocations().size() - 1;
                            double latitude = locationResult.getLocations().get(lastestLocationIndex).getLatitude();
                            double longitude = locationResult.getLocations().get(lastestLocationIndex).getLongitude();
                            textLatLong.setText(
                                    String.format(
                                            "Latitude: %s\nLongitude: %s",
                                            latitude,
                                            longitude
                                    )
                            );

                            Location location = new Location("providerNA");
                            location.setLatitude(latitude);
                            location.setLongitude(longitude);
                            String cityname = getCityNAme(location);
                            textAddress.setText(cityname);
                            campos(latitude,longitude,cityname);

                        }progressBar.setVisibility(View.GONE);

                    }
                }, Looper.getMainLooper());
    }

    private void campos(double la , double lo, String li){
        //String  text = mEditText.getText().toString();
        //Product pro = new Product("iphone 1","mola","234","22","30","carrer long");
        String nom = mEditText.getText().toString();
        String desc = mdes.getText().toString();
        String preu = mpreu.getText().toString();
        String lati = String.valueOf(la);
        String longi = String.valueOf(lo);
        Product pro = new Product(nom,desc,preu,lati,longi,li,url);
        mDatabase.child("Producto").push().setValue(pro);
        Toast.makeText(this.getContext(), "Nova oferta afegida", Toast.LENGTH_SHORT).show();

    }

    private String getCityNAme(Location lo){
        String city = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lo.getLatitude(),lo.getLongitude(),1);
            String address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getLocality();
            city = address;

            Log.d("mylog","complete"+addresses.toString());
            Log.d("mylog","complete"+address);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return city;


    }


}
