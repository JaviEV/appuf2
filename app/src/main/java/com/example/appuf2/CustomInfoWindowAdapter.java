package com.example.appuf2;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final Activity activity;

    public CustomInfoWindowAdapter(Activity activity){
        this.activity = activity;

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater().inflate(R.layout.info_point_map,null);

        Product pro = (Product) marker.getTag();

        ImageView imageView = view.findViewById(R.id.imageL);
        TextView tvTitle = view.findViewById(R.id.title);
        TextView tvDes = view.findViewById(R.id.description);

        tvTitle.setText(pro.getNom());
        tvDes.setText(pro.getDescripcio());
        Glide.with(activity).load(pro.getUrl()).into(imageView);

        return view;
    }
}
