package com.example.appuf2;

import java.io.Serializable;

public class Product implements Serializable {
    String nom;
    String descripcio;
    String preu;
    String latitud;
    String longitud;
    String direccio;
    String url;


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getPreu() {
        return preu;
    }

    public void setPreu(String preu) {
        this.preu = preu;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDireccio() {
        return direccio;
    }

    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Product(String nom, String descripcio, String preu, String latitud, String longitud, String direccio, String url) {
        this.nom = nom;
        this.descripcio = descripcio;
        this.preu = preu;
        this.latitud = latitud;
        this.longitud = longitud;
        this.direccio = direccio;
        this.url = url;

    }

    @Override
    public String toString() {
        return nom + " :  "+descripcio+" - " + preu+ " €"+"\n"+direccio;
    }

    public Product() {

    }
}
